window.addEventListener("DOMContentLoaded", function() {
    var navBar = document.querySelector(".navBar-container");
    var navBarStatus = true;
    window.addEventListener("scroll", function() {
        console.log(this.pageYOffset);
        if (this.pageYOffset >= 100) {
            if (navBarStatus) {
                navBar.classList.add("navBar-fixed")
                document.querySelector(".navBar-logo").style.height = "50%";
                // document.querySelector(".navBar-links").style.lineHeight = "50px";
                document.querySelector(".navBar-showMenu").style.lineHeight = "50px";
                document.querySelector(".navBar-logo img").style.width = "56%";
                navBarStatus = false;
            }
        } else {
            if (!navBarStatus) {
                navBar.classList.remove("navBar-fixed");
                document.querySelector(".navBar-logo").style.height = "100%";
                // document.querySelector(".navBar-links").style.lineHeight = "100px";
                document.querySelector(".navBar-showMenu").style.lineHeight = "100px";
                document.querySelector(".navBar-logo img").style.width = "100%";
                navBarStatus = true;
            }
        }
    });

    var cards = document.querySelectorAll(".card");
    var cardControl = document.querySelector(".cardControlAcivte");
    var cardControls = document.querySelectorAll(".cardControl i");
    var temp = 0;
    for (var i = 0; i < cardControls.length; i++) {
        cardControls[i].addEventListener("click", function() {
            if (this == cardControls[temp]) return;
            else {
                for (var j = 0; j < cardControls.length; j++) {
                    cardControls[j].classList.remove("cardControlActive");
                }
                for (var j = 0; j < cardControls.length; j++) {
                    if (this == cardControls[j]) {
                        if (j > temp) {
                            cards[temp].classList.add("cardRightClickCurrent");
                            cards[j].classList.add("cardRightClickNext");
                            temp = j;

                            document.querySelector(".cardRightClickNext").addEventListener("webkitAnimationEnd", function() {
                                this.classList.add("cardActive");
                                this.classList.remove("cardRightClickNext")
                            });
                            document.querySelector(".cardRightClickCurrent").addEventListener("webkitAnimationEnd", function() {
                                this.classList.remove("cardActive");
                                this.classList.remove("cardRightClickCurrent");
                            });
                            break;
                        } else {
                            cards[temp].classList.add("cardLeftClickCurrent");
                            cards[j].classList.add("cardLeftClickPre");
                            temp = j;

                            document.querySelector(".cardLeftClickPre").addEventListener("webkitAnimationEnd", function() {
                                this.classList.add("cardActive");
                                this.classList.remove("cardLeftClickPre")
                            });
                            document.querySelector(".cardLeftClickCurrent").addEventListener("webkitAnimationEnd", function() {
                                this.classList.remove("cardActive");
                                this.classList.remove("cardLeftClickCurrent");
                            });
                            break;
                        }
                    }
                }
            }
            this.classList.add("cardControlActive");
        });
    }
    var scrollTop = document.querySelector(".footerLower-scrollTop i");
    scrollTop.addEventListener("click", function() {
        $('html, body').animate({ scrollTop: 0 }, 1000);
    });

    var showMenu = document.querySelector(".navBar-showMenu");
    showMenu.addEventListener("click", function() {
        document.querySelector(".navBar-links").classList.add("navBar-links-appear");
        document.querySelector("body").style.marginLeft = '-200px';
    });
    var hideMenu = document.querySelector(".navBar-hideMenu");
    hideMenu.addEventListener("click", function() {
        document.querySelector(".navBar-links").classList.add("navBar-links-disappear");
        document.querySelector("body").style.marginLeft = '0px';
        setTimeout(function() {
            document.querySelector(".navBar-links").classList.remove("navBar-links-disappear");
            document.querySelector(".navBar-links").classList.remove("navBar-links-appear");
        }, 500);
    });

});