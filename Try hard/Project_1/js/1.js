window.addEventListener("DOMContentLoaded", function() {
    var showMenuBtn = document.querySelector(".header-show-menu");
    showMenuBtn.addEventListener("click", function() {
        document.querySelector(".header-links").classList.add("header-links-appear");
    });
    var hideMenuBtn = document.querySelector(".hide-menu");
    hideMenuBtn.addEventListener("click", function() {
        document.querySelector(".header-links").classList.remove("header-links-appear");
        document.querySelector(".header-links").classList.add("header-links-disappear");

        setTimeout(function() {
            document.querySelector(".header-links").classList.remove("header-links-disappear");
        }, 1000);
    });

    var statusHeader = true; // True means y is lower than 50px;
    var headerOffSetop = 50;
    window.addEventListener("scroll", function() {
        if (this.pageYOffset >= headerOffSetop) {
            if (statusHeader) {
                document.querySelector(".header").classList.add("header-fixed");
                document.querySelector(".content").style.marginTop = "50px";
                statusHeader = false;
            }
        } else {
            if (!statusHeader) {
                document.querySelector(".header").classList.remove("header-fixed");
                document.querySelector(".content").style.marginTop = "0px";
                statusHeader = true;
            }
        }
    });
    var links = document.querySelectorAll(".header-links li");
    for (var i = 0; i < links.length; i++) {
        if (i != links.length - 1) {
            links[i].addEventListener("click", function() {
                for (var j = 0; j < links.length; j++) {
                    if (j != i)
                        links[j].classList.remove("active");
                }
                this.classList.add("active");
            });
        }
    }

    window.addEventListener("resize", function() {
        if (document.documentElement.clientWidth >= 751) {
            document.querySelector(".header-links").classList.remove("header-links-appear");
        }
    });


    document.querySelector(".banner-next").addEventListener("click", function() {
        clearInterval(autoBanner);
        var imgactive = document.querySelector(".banner-active");
        imgactive.classList.add("banner-next-button-current");
        if (imgactive.previousElementSibling != null)
            imgactive.previousElementSibling.classList.add("banner-next-button-prev");
        else
            document.querySelector(".banner-images div:last-child").classList.add("banner-next-button-prev");
        document.querySelector(".banner-next-button-prev").addEventListener("webkitAnimationEnd", function() {
            this.classList.add("banner-active");
            this.classList.remove("banner-next-button-prev");
        });
        document.querySelector(".banner-next-button-current").addEventListener("webkitAnimationEnd", function() {
            this.classList.remove("banner-active");
            this.classList.remove("banner-next-button-current");
        });
    });

    document.querySelector(".banner-pre").addEventListener("click", function() {
        clearInterval(autoBanner);
        var imgactive = document.querySelector(".banner-active");
        imgactive.classList.add("banner-pre-button-current");
        if (imgactive.nextElementSibling != null)
            imgactive.nextElementSibling.classList.add("banner-pre-button-next");
        else
            document.querySelector(".banner-images div:first-child").classList.add("banner-pre-button-next");
        document.querySelector(".banner-pre-button-next").addEventListener("webkitAnimationEnd", function() {
            this.classList.add("banner-active");
            this.classList.remove("banner-pre-button-next");
        });
        document.querySelector(".banner-pre-button-current").addEventListener("webkitAnimationEnd", function() {
            this.classList.remove("banner-active");
            this.classList.remove("banner-pre-button-current");
        });
    });

    var autoBanner = setInterval(function() {
        var imgactive = document.querySelector(".banner-active");
        imgactive.classList.add("banner-next-button-current");
        if (imgactive.previousElementSibling != null)
            imgactive.previousElementSibling.classList.add("banner-next-button-prev");
        else
            document.querySelector(".banner-images div:last-child").classList.add("banner-next-button-prev");
        document.querySelector(".banner-next-button-prev").addEventListener("webkitAnimationEnd", function() {
            this.classList.add("banner-active");
            this.classList.remove("banner-next-button-prev");
        });
        document.querySelector(".banner-next-button-current").addEventListener("webkitAnimationEnd", function() {
            this.classList.remove("banner-active");
            this.classList.remove("banner-next-button-current");
        });
    }, 5000);

    var scrollTop = document.querySelector(".scroll-top");
    scrollTop.addEventListener("click", function() {
        $('html, body').animate({ scrollTop: 0 }, 1000);
    });

    var aboutContentButtons = document.querySelectorAll(".about-content-buttons img");
    var aboutImgs = document.querySelectorAll(".about-content-detail");
    for (var i = 0; i < aboutContentButtons.length; i++) {
        aboutContentButtons[i].addEventListener("click", function() {
            clearInterval(autoAbout);
            var imageActivated = document.querySelector(".about-content-detail-active");
            var temp;
            var temp2;
            for (var j = 0; j < aboutContentButtons.length; j++) {
                aboutContentButtons[j].classList.remove("about-content-image-active");
                if (imageActivated == aboutImgs[j]) temp = j;
                if (this == aboutContentButtons[j]) temp2 = j;
            }
            this.classList.add("about-content-image-active");
            if (this != aboutContentButtons[temp]) {
                if (temp2 > temp) {
                    aboutImgs[temp].classList.add("about-content-detail-current");
                    aboutImgs[temp2].classList.add("about-content-detail-next");
                    document.querySelector(".about-content-detail-next").addEventListener("webkitAnimationEnd", function() {
                        this.classList.add("about-content-detail-active");
                        this.classList.remove("about-content-detail-next");
                    });
                    document.querySelector(".about-content-detail-current").addEventListener("webkitAnimationEnd", function() {
                        this.classList.remove("about-content-detail-active");
                        this.classList.remove("about-content-detail-current");
                    });
                } else {
                    aboutImgs[temp].classList.add("about-content-detail-current2");
                    aboutImgs[temp2].classList.add("about-content-detail-next2");
                    document.querySelector(".about-content-detail-next2").addEventListener("webkitAnimationEnd", function() {
                        this.classList.add("about-content-detail-active");
                        this.classList.remove("about-content-detail-next2");
                    });
                    document.querySelector(".about-content-detail-current2").addEventListener("webkitAnimationEnd", function() {
                        this.classList.remove("about-content-detail-active");
                        this.classList.remove("about-content-detail-current2");
                    });
                }
            }
        });
    }

    //From top to bottom
    // var autoAbout = setInterval(function() {
    //     var imageActivated = document.querySelector(".about-content-detail-active");
    //     var temp;
    //     for (var j = 0; j < aboutContentButtons.length; j++) {
    //         aboutContentButtons[j].classList.remove("about-content-image-active");
    //         if (imageActivated == aboutImgs[j]) temp = j;
    //     }
    //     aboutContentButtons[(temp + 1 <= (aboutContentButtons.length - 1)) ? (temp + 1) : 0].classList.add("about-content-image-active");
    //     if (this != aboutContentButtons[temp]) {
    //         aboutImgs[temp].classList.add("about-content-detail-current");
    //         if (temp + 1 <= aboutContentButtons.length - 1)
    //             aboutImgs[temp + 1].classList.add("about-content-detail-next");
    //         else
    //             aboutImgs[0].classList.add("about-content-detail-next");
    //         document.querySelector(".about-content-detail-next").addEventListener("webkitAnimationEnd", function() {
    //             this.classList.add("about-content-detail-active");
    //             this.classList.remove("about-content-detail-next");
    //         });
    //         document.querySelector(".about-content-detail-current").addEventListener("webkitAnimationEnd", function() {
    //             this.classList.remove("about-content-detail-active");
    //             this.classList.remove("about-content-detail-current");
    //         });
    //     }
    // }, 3000);


    //top to bottom and bottom to top
    var direc = "tTB";
    var autoAbout = setInterval(function() {
        var imageActivated = document.querySelector(".about-content-detail-active");
        var temp;
        for (var j = 0; j < aboutContentButtons.length; j++) {
            aboutContentButtons[j].classList.remove("about-content-image-active");
            if (imageActivated == aboutImgs[j]) temp = j;
        }
        console.log(direc);
        if (temp + 1 <= aboutContentButtons.length - 1 && direc == "tTB") {
            aboutImgs[temp].classList.add("about-content-detail-current");
            temp = temp + 1;
            aboutImgs[temp].classList.add("about-content-detail-next");
            document.querySelector(".about-content-detail-next").addEventListener("webkitAnimationEnd", function() {
                this.classList.add("about-content-detail-active");
                this.classList.remove("about-content-detail-next");
            });
            document.querySelector(".about-content-detail-current").addEventListener("webkitAnimationEnd", function() {
                this.classList.remove("about-content-detail-active");
                this.classList.remove("about-content-detail-current");
            });
            if (temp == aboutContentButtons.length - 1) direc = "bTT";
        } else {
            aboutImgs[temp].classList.add("about-content-detail-current2");
            temp = temp - 1;
            aboutImgs[temp].classList.add("about-content-detail-next2");
            document.querySelector(".about-content-detail-next2").addEventListener("webkitAnimationEnd", function() {
                this.classList.add("about-content-detail-active");
                this.classList.remove("about-content-detail-next2");
            });
            document.querySelector(".about-content-detail-current2").addEventListener("webkitAnimationEnd", function() {
                this.classList.remove("about-content-detail-active");
                this.classList.remove("about-content-detail-current2");
            });
            if (temp == 0) direc = "tTB";
        }
        aboutContentButtons[temp].classList.add("about-content-image-active");
    }, 3000);
})