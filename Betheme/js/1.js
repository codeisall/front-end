window.addEventListener("DOMContentLoaded", function(){
	var temp;
	//Get Buy now button 
	var buyNowButton = document.querySelectorAll(".navBarRigth_rightButton");
	//Get links in nav bar
	var navBarLinks = document.querySelectorAll("ul.navBaright_leftLinks li a");
	//Get search icon on nav bar
	var searchIcon = document.querySelector(".navBarRight_rightSearchIcon");
	//Get close icon in search box
	var closeIcon = document.querySelector(".searchContainer_closeIcon");
	//Get 3e cols in first content container
	var firstContentCols = document.querySelectorAll(".firstContentRow .col");
	//Get nav bar
	var navBar = document.querySelector(".navBar");
	var status = false;
	//Get scrollTop icon
	var scrollTop = document.querySelector(".scrollToTop");
	//Get websites button;
	var websiteButton = document.querySelector(".websites_button");
	//Get websites content;
	var websiteContent = document.querySelector(".websites_content");

	//Get the number in pre-built purpouse
	var preBuilt = document.getElementById("secondContentRow_2_col_title_number_preBuilt");
	//Get the number in shortcodes
	var shortCodes = document.getElementById("secondContentRow_2_col_title_number_shortCodes");
	//Get the number in happy clients
	var happyClients = document.getElementById("secondContentRow_2_col_title_number_happyClients");
	//Get the number in header styles
	var headerStyles = document.getElementById("secondContentRow_2_col_title_number_headerStyles");
	var statusAutoInscrease = false;
	//Get images inside the fourth content
	var fourthContent_images = document.querySelectorAll(".fourthContentCol");
	var fifthContent_images = document.querySelectorAll(".fifthContent_card_effect");
	console.log(fifthContent_images);
	//Get buttons inside the fourth content
	var fourthContent_buttons = document.querySelectorAll(".fourthContentCol_buttons_col");

	//Buttons inside the fourth content when user hover
	for (var i = 0; i < fourthContent_buttons.length; i++){
		fourthContent_buttons[i].addEventListener("mouseover", function(){
			this.classList.add("fourthContentCol_buttons_col_hover");
			this.parentNode.parentNode.previousElementSibling.classList.add("fourthContentCol_image_hover");
		});
		fourthContent_buttons[i].addEventListener("mouseout", function(){
			this.classList.remove("fourthContentCol_buttons_col_hover");
			this.parentNode.parentNode.previousElementSibling.classList.remove("fourthContentCol_image_hover");
		});
	}
	//
	
	//Images inside the fourth content when user hover
	for (var i = 0; i < fourthContent_images.length; i++){
		fourthContent_images[i].addEventListener("mouseover", function(){
			this.childNodes[1].classList.add("fourthContentCol_image_hover");
			this.childNodes[3].classList.add("fourthContentCol_buttons_appear");
		});
		fourthContent_images[i].addEventListener("mouseout", function(){
			this.childNodes[1].classList.remove("fourthContentCol_image_hover");
			this.childNodes[3].classList.remove("fourthContentCol_buttons_appear");
		});
	}
	for (var i = 0; i < fifthContent_images.length; i++){
		fifthContent_images[i].addEventListener("mouseover", function(){
			this.childNodes[1].classList.add("fifthContent_card_effect_image_hover");
			this.childNodes[3].classList.add("fourthContentCol_buttons_appear");
		});
		fifthContent_images[i].addEventListener("mouseout", function(){
			this.childNodes[1].classList.remove("fifthContent_card_effect_image_hover");
			this.childNodes[3].classList.remove("fourthContentCol_buttons_appear");
		});
	}
	//

	window.addEventListener("scroll", function(){
		if (this.pageYOffset < navBar.offsetTop+50){
			if (status) {
				document.querySelector(".banner").style.marginTop = "0px";
				navBar.classList.remove("navBarFixed");
				status = false;
			}
		}
		else {
			if (!status){
				document.querySelector(".banner").style.marginTop = "90px";
				navBar.classList.add("navBarFixed");
				status = true;
			}
		}

		if (this.pageYOffset > 500){
			if (!statusAutoInscrease){
				var t1 = setInterval(function(){
					preBuilt.innerHTML = parseInt(preBuilt.innerHTML)+1;;
					if (parseInt(preBuilt.innerHTML) == 450)
						clearInterval(t1);
					statusAutoInscrease = true;
				}, 1);
				var t2 = setInterval(function(){
					shortCodes.innerHTML = parseInt(shortCodes.innerHTML)+1;;
					if (parseInt(shortCodes.innerHTML) == 200)
						clearInterval(t2);
					statusAutoInscrease = true;
				}, 5);
				var t3 = setInterval(function(){
					happyClients.innerHTML = parseInt(happyClients.innerHTML)+1;;
					if (parseInt(happyClients.innerHTML) == 100)
						clearInterval(t3);
					statusAutoInscrease = true;
				}, 10);
				var t4 = setInterval(function(){
					headerStyles.innerHTML = parseInt(headerStyles.innerHTML)+1;;
					if (parseInt(headerStyles.innerHTML) == 11)
						clearInterval(t4);
					statusAutoInscrease = true;
				}, 200);
			}

		}
	});
	
	//Make an effect when user hover one of them
	for (var i = 0; i < firstContentCols.length; i++){
		firstContentCols[i].addEventListener("mouseover", function(){
			this.childNodes[1].classList.add("hoverCol");
		});
		firstContentCols[i].addEventListener("mouseout", function(){
			this.childNodes[1].classList.remove("hoverCol");
		});
	}
	//

	//Open search box when user click on search icon
	searchIcon.addEventListener("click", function(){
		document.querySelector(".searchContainer").classList.toggle("searchContainerAppear");
	});
	closeIcon.addEventListener("click", function(){
		document.querySelector(".searchContainer").classList.toggle("searchContainerAppear");
	});
	//

	//Change color of the link in nav bar when user click on it
	for (var i = 0; i < navBarLinks.length; i++){
		navBarLinks[i].addEventListener("click", function(){
			for (var j = 0; j < navBarLinks.length; j++){
				if (j != i) navBarLinks[j].classList.remove("active");
			}
			this.classList.add("active");
		});
	}
	//

	//Hover on Buy now button
	for (var i = buyNowButton.length - 1; i >= 0; i--) {
		buyNowButton[i].addEventListener("mouseover", function(){
			this.childNodes[0].classList.add("blurAppear");
		});
		buyNowButton[i].addEventListener("mouseout", function(){
			this.childNodes[0].classList.remove("blurAppear");
		})
	}
	//

	//Click scroll to top
	scrollTop.addEventListener("click", function(){
		$('html, body').animate({scrollTop:0}, 1000);
	});
	//

	//Click on websites button
	websiteButton.addEventListener("click", function(){
		this.classList.toggle("websites_button_clicked");
		websiteContent.classList.toggle("websites_content_clicked");
	});
	//
});