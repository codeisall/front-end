window.addEventListener('DOMContentLoaded', (event) => {
	//Hide video congrats usa
	setTimeout(function(){
		var video = document.querySelector(".congratsUsa");
		video.classList.add("congratsUsaHide");
	}, 5000);
	//

	//Get search icon
	var searchIcon = document.querySelector(".searchIcon");
	//Get cart icon
	var cartIcon = document.querySelector(".cartIcon");
	//temp variable for remembering the first link in menu
	var temp;
	//Get blur element
	var blurBackground = document.querySelector(".blur");
	//Get result content
	var resultContent = document.querySelector(".resultContent");
	//Get headerSearch container
	var headerSearchContainer = document.querySelector(".headerSearch");
	//Code handle for clicking  search icon
	searchIcon.addEventListener("click", (event) => {
		searchIcon.classList.add("linkDisappear");
		searchIcon = searchIcon.previousElementSibling;

		//Handle result content appear
		resultContent.classList.add("resultAppear");
		//

		//Handle blur background
		blurBackground.classList.add("blurAppear");
		//

		//Header search container qppear when user click search icon
		headerSearchContainer.classList.add("headerSearchAppear");
		//

		//Change cart icon to x icon when user click onto search icon
		cartIcon.classList.replace("fa-shopping-cart", "fa-times");
		//

		var setInterValFunc = setInterval(function(){
				aFunc(searchIcon);
			}, 80);

		var aFunc = function(link){
			link.classList.add("linkDisappear");
			temp = searchIcon;
			searchIcon = searchIcon.previousElementSibling;
			if (searchIcon == null) {
				searchIcon = document.querySelector(".searchIcon");
				clearInterval(setInterValFunc);
			}
		}
	});

	//Handle click event when user click onto x icon
	cartIcon.addEventListener("click", (event) => {
		
		temp.classList.remove("linkDisappear");
		temp = temp.nextElementSibling;

		resultContent.classList.remove("resultAppear");

		blurBackground.classList.remove("blurAppear");

		headerSearchContainer.classList.remove("headerSearchAppear");

		//Change cart icon to x icon when user click onto search icon
		cartIcon.classList.replace("fa-times", "fa-shopping-cart");
		//

		var setInterValFunc = setInterval(function(){
				aFunc(temp);
			}, 100);

		var aFunc = function(link){
			link.classList.remove("linkDisappear");
			temp = temp.nextElementSibling;
			if (temp == null) {
				searchIcon = document.querySelector(".searchIcon");
				clearInterval(setInterValFunc);
			}
		}
	});
	//

	//Handle click event of blur background when user click on this instead of click on x icon
	blurBackground.addEventListener("click", (event) => {
		temp.classList.remove("linkDisappear");
		temp = temp.nextElementSibling;

		resultContent.classList.remove("resultAppear");

		blurBackground.classList.remove("blurAppear");

		headerSearchContainer.classList.remove("headerSearchAppear");

		//Change cart icon to x icon when user click onto search icon
		cartIcon.classList.replace("fa-times", "fa-shopping-cart");
		//

		var setInterValFunc = setInterval(function(){
				aFunc(temp);
			}, 100);

		var aFunc = function(link){
			link.classList.remove("linkDisappear");
			temp = temp.nextElementSibling;
			if (temp == null) {
				searchIcon = document.querySelector(".searchIcon");
				clearInterval(setInterValFunc);
			}
		}
	});
});